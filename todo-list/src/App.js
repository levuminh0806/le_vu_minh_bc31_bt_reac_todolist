// import logo from "./logo.svg";
import { useState } from "react";
import "./App.css";
function App() {
  const storage = JSON.parse(localStorage.getItem("jobs"));

  const [job, setJob] = useState("");
  const [jobs, setJobs] = useState(storage ?? []);
  const hanldeplus = () => {
    setJobs((prev) => {
      const newJobs = [...prev, job];
      const jsonJobs = JSON.stringify(newJobs);
      localStorage.setItem("json", jsonJobs);
      return newJobs;
    });
    setJob("");
  };
  return (
    <div className="App">
      <div className="card" style={{ width: "18rem" }}>
        <img
          src="https://cdn.pixabay.com/photo/2020/09/24/16/50/board-5599231__480.png"
          className="card-img-top"
          alt="..."
        />
        <div className="card-body">
          <h5 className="card-title">ToDo List</h5>
          <div className="header">
            <div className="form-group">
              <label htmlFor />
              <input
                type="text"
                className="form-control"
                name
                id
                aria-describedby="helpId"
                placeholder
                value={job}
                onChange={(e) => setJob(e.target.value)}
              />
            </div>
            <button className="btn btn-primary" onClick={hanldeplus}>
              {" "}
              +
            </button>
          </div>
          <div className="show">
            <ul>
              {jobs.map((job, index) => (
                <li key={index}>
                  {job}
                  <i class="fa-solid fa-trash-can"></i>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
